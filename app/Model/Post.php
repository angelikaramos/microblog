<?php
App::uses('AppModel', 'Model');

class Post extends AppModel
{
    public $validate = array(
        'post_text' => array(
            'required' => array(
                'rule' => 'notBlank',
            ),
            'This post is limited to 140 characters only' => array(
                'rule' => array('maxLength', 140),
            ),
        )
    );

    // belongsTo Association
    public $belongsTo = 'User';

    // hasMany Association
    public $hasMany = array(
        'Repost' => array(
            'className' => 'Repost'
        )
    );
}

           
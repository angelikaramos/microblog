<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel
{
    public $validate = array(
        'full_name' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => '*Please enter your full name.'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => '*Please enter your email.'
            )
        ),
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => '*Please enter your username.'
            ),
            'between' => array(
                'rule' => array('lengthBetween', 5, 15),
                'message' => '*Username must be atleast 5 and less than 15 characters long.',
            ),
            'That username is already existing' => array(
                'rule' => 'isUnique',
                'message' => '*That username is already existing.',
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => '*Please enter your password.'
            ),
            'The password must be atleast 8 characters long' => array(
                'rule' => array('minLength', 8),
                'message' => '*Password must be atleast 8 characters long',
            )
        )
    );

    // hasMany Assocation
    public $hasMany = array(
        'Post' => array(
            'className' => 'Post'
        ),
        'Follower' => array(
            'className' => 'Follower'
        ),
        'Repost' => array(
            'className' => 'Repost',
            'foreignKey' => 'reposter_user_id'
        ),
        'Comment' => array(
            'className' => 'Comment',
            'foreignKey' => 'commenter_user_id'
        ),
    );

    /**
     * Executes immediately after model has validated but before save the password hashed
     * @param $options = array()
     * @return bool
     */
    public function beforeSave($options = array())
    {
       if (isset($this->data[$this->alias]['password'])) {
           $passwordHasher = new BlowfishPasswordHasher();
           $this->data[$this->alias]['password'] = $passwordHasher->hash(
               $this->data[$this->alias]['password']
           );
       }
       return true;
    }
}

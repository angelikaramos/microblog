<?php
App::uses('AppModel', 'Model');

class Repost extends AppModel
{
    // belongsTo Association
    public $belongsTo = array(
        'Post' => array(
            'className' => 'Post',
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'reposter_user_id'
        )
    );
}
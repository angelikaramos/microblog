<?php
App::uses('AppModel', 'Model');

class Comment extends AppModel
{
    public $validate = array(
        'comment_text' => array(
            'required' => array(
                'rule' => 'notBlank',
            )
        )
    );

    // belongsTo Association
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'commenter_user_id'
        ),
    );
}
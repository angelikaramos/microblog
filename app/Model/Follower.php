<?php
App::uses('AppModel', 'Model');

class Follower extends AppModel
{
    // belongsTo Assocation
    public $belongsTo = 'User';
}
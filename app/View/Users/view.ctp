<!-- File: /app/View/Users/view.ctp -->
<?php 
    // Get user data by user_id
    $user = $this->requestAction('users/getUserById/' . $user_id);
?>
<!-- Navigation Bar -->
<?php echo $this->element('navbar'); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <!-- Side Navigation Bar -->
        <?php echo $this->element('side_navbar'); ?>
    </div>
    <div id="layoutSidenav_content">
        <!-- Flash message -->
        <?php echo $this->Flash->render(); ?>
        <main>
            <div class="container-fluid">
                <!-- Edit Profile Button -->
                <?php if (AuthComponent::user('id') === $user_id): ?>
                    <?php echo $this->Html->Link('Edit Profile', array('controller' => 'users', 
                    'action' => 'edit', $user_id), array('class' => 'btn btn-primary float-right mt-3')) ?>
                <?php endif; ?>
                <div class="row">
                    <!-- User -->
                    <div class="col-md-2 mt-3">
                    <?php echo $this->Html->image('uploads/' . $user['User']['profile_picture'], array('alt' => 'Profile Picture', 'height' => '150px')); ?>
                    </div>
                    <div class="col-md-10 mt-5">
                        <span class="user-fullname h2"><?php echo h($user['User']['full_name']); ?></span>
                        <?php echo "@" . h($user['User']['username']); ?>
                        <p class="small"><?php echo "Joined " . date('F Y', strtotime($user['User']['created'])); ?></p>
                        <p><b>Posts:</b> <?php echo $this->requestAction('posts/countPosts/' . $user_id);?>
                        | <b>Followers:</b> <?php echo $this->requestAction('followers/countFollowers/' . $user_id); ?>
                        | <b>Following:</b> <?php echo $this->requestAction('followers/countFollowing/' . $user_id); ?>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="posts">
                            <!-- If no posts -->
                            <?php 
                                if (empty($posts)) {
                                    echo "<p class='text-center'> No posts yet. </p>";
                                }
                            ?>
                            <?php foreach ($posts as $post): ?>
                                <div class="post">
                                    <!-- Add dropdown_post element if logged in user is the one who post -->
                                    <?php 
                                        if (AuthComponent::user('id') == $post['User']['id']) {
                                            echo $this->element('dropdown_post', array('post' => $post)); 
                                        }
                                    ?>
                                    <!-- Repost label -->
                                    <span class="small text-primary">
                                        <?php 
                                            // Check if this certain post was reposted
                                            $repost = $this->requestAction('reposts/checkRepost/' . $post['Post']['id'] . '/' . $user_id);
                                            // Check first if this is the view of the logged in user
                                            if ($user_id === AuthComponent::user('id')) {
                                                // Then, Check if this certain post was reposted by logged in user
                                                $repost = $this->requestAction('reposts/checkRepostOfLoggedInUser/' . $post['Post']['id']);
                                                // If reposted by user
                                                if ($repost === 1) {
                                                    // Get post_id of repost
                                                    $getRepost = $this->requestAction('reposts/getRepostOfLoggedInUser/' . $post['Post']['id'] . '/' . $user_id);
                                                    // Check if the reposter_user_id is the logged in user
                                                    $reposter = $requestAction('reposts/getReposterUserId/' . $getRepost['Repost']['id']);
                                                    if ($reposter['Repost']['reposter_user_id'] === $user_id) {
                                                        echo " <i class='fa fa-retweet' aria-hidden='true'></i> ";
                                                        echo "You reposted";
                                                    }
                                                }
                                            } elseif ($repost >= 1) {
                                                // If there is a reposter, check if it is equal to the other user
                                                $getRepost = $this->requestAction('reposts/getRepost/' . $post['Post']['id'] . '/' . $user_id);
                                                if ($getRepost['Repost']['reposter_user_id'] === $user_id) {
                                                    echo "<i class='fa fa-retweet' aria-hidden='true'></i> Reposted by ";
                                                    echo $getRepost['User']['username'];
                                                } 
                                            } 
                                        ?>
                                    </span>
                                    <!-- Post user -->
                                    <div class="post-user">
                                        <?php echo $this->Html->image(
                                            'uploads/' . $post['User']['profile_picture'], array('alt' => 'Profile Picture', 'height' => '50px')
                                        ); ?>
                                        <?php echo $this->Html->link($post['User']['full_name'], array('controller' => 'users',
                                        'action' => 'view', $post['User']['id']), array('class' => 'text-decoration-none text-dark font-weight-bold')) ?>
                                        <span><?php echo "@" . h($post['User']['username']); ?></span>
                                    </div>
                                    <!-- Post text -->
                                    <p class="post-content">
                                        <?php echo h($post['Post']['post_text']); ?>
                                    </p>
                                    <!-- Like, Comment and Repost Buttons -->
                                    <div class="btn-group-sm float-right">
                                        <?php echo '<div class="d-inline p-2 bg-primary" id="btnLike'.$post['Post']['id'].'">'; ?>
                                            <?php $loggedInUserLike = $this->requestAction('likes/checkLikeOfLoggedInUser/' . $post['Post']['id']); ?>
                                            <?php $countLikes = $this->requestAction('likes/countLikes/' . $post['Post']['id']); ?>
                                            <?php $countLikes <= 1 ? $btn = 'Like' : $btn = 'Likes'; ?>
                                            <?php if ($loggedInUserLike === 0) : ?>
                                                <?php echo "<span class='text-white'>" . "(" . $countLikes . ")" . "</span> "; ?>
                                                <?php 
                                                    echo $this->Js->link($btn, array('controller' => 'likes', 
                                                    'action' => 'add', $post['Post']['id']) , array('update' => '#btnLike'.$post['Post']['id'].'', 'class' => 'text-decoration-none text-white')); 
                                                ?>
                                            <?php endif; ?>    
                                            <?php if ($loggedInUserLike === 1) : ?>
                                                <?php echo "<span class='text-white'>" . "(" . $countLikes = $this->requestAction('likes/countLikes/' . $post['Post']['id']). ")" . "</span> "; ?>
                                                <?php 
                                                    echo $this->Js->link($btn, array('controller' => 'likes', 
                                                    'action' => 'delete', $post['Post']['id']), array('update' => '#btnLike'.$post['Post']['id'].'', 'class' => 'text-decoration-none text-white')); 
                                                ?>
                                            <?php endif; ?>  
                                        </div>
                                        <div class="d-inline p-2 bg-dark">
                                            <?php $countComments = $this->requestAction('comments/countComments/' . $post['Post']['id']); ?>
                                            <?php $countComments <= 1 ? $btnComment = 'Comment' : $btnComment = 'Comments'; ?>
                                            <?php echo "<span class='text-white'>" . "(" . $countComments . ")" . "</span> "; ?>
                                            <?php echo $this->Form->postLink($btnComment, array('controller' => 'posts', 
                                            'action' => 'view',  $post['Post']['id']), array('class' => 'text-decoration-none text-white')) ?>
                                        </div>
                                        <div class="d-inline p-2 bg-info">
                                            <?php $loggedInUserRepost = $this->requestAction('reposts/checkRepostOfLoggedInUser/' . $post['Post']['id'] . '/' . AuthComponent::user('id')); ?>
                                            <?php $countReposts = $this->requestAction('reposts/countReposts/' . $post['Post']['id']); ?>
                                            <?php $countReposts <= 1 ? $btnRepost = 'Repost' : $btnRepost = 'Reposts'; ?>
                                            <?php echo "<span class='text-white'>" . "(" . $countReposts . ")" . "</span> "; ?>
                                            <?php if ($loggedInUserRepost === 1) : ?>
                                                <?php echo $this->Form->postLink($btnRepost, array('controller' => 'reposts', 
                                                'action' => 'delete',  $post['Post']['id']), array('class' => 'text-decoration-none text-white',
                                                'confirm' => 'Are you sure you want to undo repost?')) ?>
                                            <?php endif; ?>
                                            <?php if ($loggedInUserRepost === 0) : ?>
                                                <?php echo $this->Form->postLink($btnRepost, array('controller' => 'reposts', 
                                                'action' => 'add',  $post['Post']['id']), array('class' => 'text-decoration-none text-white')) ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <!-- Post date -->
                                    <p class="post-date small"><?php echo date('g:i A - j M Y', strtotime($post['Post']['modified'])); ?></p>
                                </div><hr>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Pagination -->
        <?php echo $this->element('pagination'); ?>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
    </div>
</div>

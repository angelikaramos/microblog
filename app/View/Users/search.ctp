<!-- File: /app/View/Users/search.ctp -->
<?php 
App::import('Controller', 'Users');
App::import('Controller', 'Posts');
App::import('Controller', 'Reposts');
App::import('Controller', 'Followers');
App::import('Controller', 'Likes');

// Controllers
$usersController = new UsersController();
$postsController = new PostsController();
$repostsController = new RepostsController();
$followersController = new FollowersController();
$likesController = new LikesController();

// Get user's not yet followed
$usersNotFollowed = $usersController->getUsersNotYetFollowed();
?>
<!-- Navigation Bar -->
<?php echo $this->element('navbar'); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <!-- Side Navigation Bar -->
        <?php echo $this->element('side_navbar'); ?>
    </div>
    <div id="layoutSidenav_content">
        <!-- Flash message -->
        <?php echo $this->Flash->render(); ?>
        <main>
            <div class="container-fluid">
                <h2 class="mt-4">Search Result  : <?php echo $keyword; ?></h2>
                <?php 
                    if (empty($data)) {
                        echo "<p class='text-center'> No results found.</p>";
                    }
                ?>
                <!-- Search results -->
                <?php foreach ($data as $result): ?>
                    <div class="following mt-4">
                        <!-- User -->
                        <?php echo $this->Html->image(
                            'uploads/' . $result['User']['profile_picture'], array('alt' => 'Profile Picture', 'height' => '50px')
                        ); ?>
                        <strong><?php echo $this->Html->link($result['User']['full_name'], array('controller' => 'users',
                        'action' => 'view', $result['User']['id']), array('class' => 'text-decoration-none text-dark')) ?></strong>
                        <span><?php echo "@" . $result['User']['username']; ?></span>
                        <?php 
                            $userfollowing = $followersController->checkIfFollowing($result['User']['id']);
                        ?>
                        <!-- Follow or Following Button -->
                        <?php if (!empty($userfollowing)) : ?>
                            <?php if ($userfollowing['Follower']['_is_deleted'] === '1') : ?>
                                <?php echo $this->Form->postLink('Follow', array('controller' => 'followers', 'action' => 'add', $result['User']['id'], '?' => array('keyword' => $keyword)), 
                                array('class' => 'btn btn-primary float-right',)) ?>
                            <?php endif; ?>
                            <?php if ($userfollowing['Follower']['_is_deleted'] === '0') : ?>
                                <?php echo $this->Form->postLink('Following', array('controller' => 'followers', 
                                'action' => 'delete',  $userfollowing['Follower']['id'], '?' => array('keyword' => $keyword)), array('class' => 'btn btn-primary active float-right',
                                'confirm' => 'Are you sure you want to unfollow this person?')) ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if (empty($userfollowing)) : ?>
                            <?php echo $this->Form->postLink('Follow', array('controller' => 'followers', 'action' => 'add', $result['User']['id'], '?' => array('keyword' => $keyword)), 
                                array('class' => 'btn btn-primary float-right',)) ?>
                        <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </main>
        <!-- Pagination -->
        <?php echo $this->element('pagination'); ?>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
    </div>
</div>

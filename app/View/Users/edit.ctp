<!-- File: /app/View/Users/edit.ctp -->
<!-- Navigation Bar -->
<?php echo $this->element('navbar'); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <!-- Side Navigation Bar -->
        <?php echo $this->element('side_navbar'); ?>
    </div>
    <div id="layoutSidenav_content">
        <!-- Flash message -->
        <?php echo $this->Flash->render(); ?>
        <main>
            <div class="container-fluid">
                <h2 class="mt-4 mb-5">Edit Profile</h2>
                <?php
                    echo $this->Form->create(null,  ['type' => 'file'], array(
                        'inputDefaults' => array(
                            'label' => false,
                        ),
                    ));
                ?>
                    <?php
                        echo $this->Form->input('username', array(
                            'class' => 'form-control mb-3',
                        ));
                    ?>
                    <?php
                        echo $this->Form->input('email', array(
                            'class' => 'form-control mb-3',
                        ));
                    ?>
                    <?php
                        echo $this->Form->input('change_password', array(
                            'class' => 'form-control mb-3',
                            'placeholder' => 'Password',
                            'type' => 'password',
                        ));
                    ?>
                    <?php 
                        echo $this->Form->file('profile_picture', ['type'=>'file'], array(
                            'class' => 'form-control mb-3',
                        )); 
                    ?>
                    <?php
                        $submit = array(
                            'label' => 'Submit',
                            'class' => 'btn btn-primary float-right text-uppercase'
                        );
                    ?>
                <?php echo $this->Form->end($submit); ?>
            </div>
        </main>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
    </div>
</div>

<!-- app/View/Users/login.ctp -->
<?php echo $this->Flash->render('auth'); ?>
<div class="login-container d-flex align-items-center justify-content-center">
    <?php
        echo $this->Form->create('User', array(
            'inputDefaults' => array(
                'label' => false,
            )
        ));
    ?>
        <h4 class="mb-5 text-center"> LOGIN </h4>
        <?php
            echo $this->Form->input('username', array(
                'class' => 'form-control rounded-pill form-control-lg mb-3',
                'placeholder' => 'Username',
                'required' => false)
            );
        ?>
        <?php
            echo $this->Form->input('password', array(
                'class' => 'form-control rounded-pill form-control-lg mb-3',
                'placeholder' => 'Password',
                'required' => false)
            );
        ?>
        <p class="mt-3 font-weight-normal">
            Don't have account yet? 
            <?php echo $this->Html->link('Click here', array(
                'controller'=>'users','action'=>'add'
                )); 
            ?> 
            to register
        </p>
        <?php
            $submit = array(
                'label' => 'Login',
                'class' => 'btn btn-primary text-uppercase btn-block
                rounded-pill btn-lg'
            );
        ?>
        <?php echo $this->Form->end($submit); ?>
    </form>
</div>
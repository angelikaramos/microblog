<div class="alert alert-success" role="alert">
    <h4 class="alert-heading">Your account has already activated!</h4>
    <hr>
    <p class="mt-3 font-weight-normal mb-0">
        Please login
        <?php echo $this->Html->link('here.', array(
            'controller'=>'users',
            'action'=>'login'
            )); 
        ?> 
    </p>
</div>
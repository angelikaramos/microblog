<!-- app/View/Users/add.ctp -->
<div class="login-container d-flex align-items-center justify-content-center">
    <?php
        echo $this->Form->create('User', array(
            'inputDefaults' => array(
                'label' => false,
                'div' => 'text-danger',
            ),
        ));
    ?>
        <h4 class="mb-5 text-center"> REGISTRATION </h4>
        <?php 
            echo $this->Form->input('full_name', array(
                'class' => 'form-control rounded-pill form-control-lg mb-3',
                'placeholder' => 'Full name',
                'required' => false)
            );
            echo $this->Form->input('email', array(
                'class' => 'form-control rounded-pill form-control-lg mb-3',
                'placeholder' => 'Email',
                'required' => false)
            );
            echo $this->Form->input('username', array(
                'class' => 'form-control rounded-pill form-control-lg mb-3',
                'placeholder' => 'Username',
                'required' => false)
            );
            echo $this->Form->input('password', array(
                'class' => 'form-control rounded-pill form-control-lg mb-3',
                'placeholder' => 'Password',
                'required' => false)
            );
        ?>
        <p class="mt-3 font-weight-normal">
            Already have an account? 
            <?php echo $this->Html->link('Click here', array(
                'controller'=>'users',
                'action'=>'login'
                )); 
            ?> 
            to login
        </p>
        <?php
            $submit = array(
                'label' => 'Register',
                'class' => 'btn btn-primary text-uppercase btn-block 
                rounded-pill btn-lg'
            );
        ?>
    <?php echo $this->Form->end($submit); ?>
</div>
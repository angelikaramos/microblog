<!-- File: /app/View/Posts/index.ctp -->
<?php 
    // Get logged in user_id
    $user_id = AuthComponent::user('id');
?>
<!-- Navigation Bar -->
<?php echo $this->element('navbar'); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <!-- Side Navigation Bar -->
        <?php echo $this->element('side_navbar'); ?>
    </div>
    <div id="layoutSidenav_content">
        <!-- Flash message -->
        <?php echo $this->Flash->render(); ?>
        <main>
            <div class="container-fluid">
                <div class="row">
                    <!-- Add Post -->  
                    <div class="col-md-10 mt-3">
                        <?php
                            echo $this->Form->create('Post', array(
                                'url' => 'add'), array(
                                    'inputDefaults' => array(
                                        'label' => false,
                                        'div' => 'text-danger',
                                    )
                                )
                            );
                        ?>
                        <?php
                            echo $this->Form->textarea('post_text', array(
                                'cols' => '25',
                                'rows' => '3',
                                'id' => 'post_text',
                                'class' => 'form-control mb-3',
                                'placeholder' => 'Share your thoughts here..',
                                'required' => false,
                            ));
                        ?>
                    </div>
                    <div class="col-md-2 mt-5">
                        <?php
                            $submit = array(
                                'label' => 'Post',
                                'class' => 'form-control btn btn-primary text-uppercase',
                            );
                        ?>
                        <?php echo $this->Form->end($submit); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12"><hr>
                        <div class="posts">
                            <!-- If no posts -->
                            <?php 
                                if (empty($posts)) {
                                    echo "<p class='text-center'> No posts yet. </p>";
                                }
                            ?>
                            <?php foreach ($posts as $post): ?>
                                <div class="post">
                                    <!-- Add dropdown_post element if logged in user is the one who post -->
                                    <?php 
                                        if (AuthComponent::user('id') == $post['User']['id']) {
                                            echo $this->element('dropdown_post', array('post' => $post)); 
                                        }
                                    ?>
                                    <!-- Repost label -->
                                    <?php echo $this->element('repost', array('post_id' => $post['Post']['id'], 'user_id' => $user_id)); ?>
                                    <!-- Post user -->
                                    <div class="post-user">
                                        <?php echo $this->Html->image('uploads/' . $post['User']['profile_picture'], 
                                        array('alt' => 'Profile Picture', 'height' => '50px')); ?>
                                        <span class="font-weight-bold">
                                            <?php echo $this->Html->link(
                                                $post['User']['full_name'], array(
                                                'controller' => 'users',
                                                'action' => 'view', $post['User']['id']), array(
                                                'class' => 'text-decoration-none text-dark font-weight-bold')
                                            ); ?>
                                        </span>
                                        <span><?php echo "@" . h($post['User']['username']); ?></span>
                                    </div>
                                    <!-- Post text -->
                                    <p class="post-content"><?php echo h($post['Post']['post_text']); ?></p>
                                    <!-- Like, Comment and Repost Button -->
                                    <?php echo $this->element('buttons', array('post_id' => $post['Post']['id'], 'user_id' => $user_id)); ?>
                                    <!-- Post date -->
                                    <p class="post-date small"><?php echo date('g:i A - j M Y', strtotime($post['Post']['modified'])); ?></p>
                                </div><hr>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Pagination -->
        <?php echo $this->element('pagination'); ?>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
    </div>
</div>


<!-- File: /app/View/Posts/edit.ctp -->
<!-- Navigation Bar -->
<?php echo $this->element('navbar'); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <!-- Side Navigation Bar -->
        <?php echo $this->element('side_navbar'); ?>
    </div>
    <div id="layoutSidenav_content">
        <!-- Flash message -->
        <?php echo $this->Flash->render(); ?>
        <main>
            <div class="container-fluid">
                <!-- Post user -->
                <div class="post-user mt-3">
                    <?php echo $this->Html->image(
                        'uploads/' . $post['User']['profile_picture'], array(
                        'alt' => 'Profile Picture', 'height' => '50px')
                    ); ?>
                    <span class="font-weight-bold"><?php echo $post['User']['full_name']; ?></span>
                    <span><?php echo "@" . $post['User']['username']; ?></span>
                </div>
                <div class="row">
                    <!-- Edit Post -->  
                    <div class="col-md-10 mt-3">
                        <?php
                            echo $this->Form->create('Post', array(
                                'url' => array($post['Post']['id']),
                                'inputDefaults' => array(
                                    'label' => false,
                                )
                            ));
                        ?>
                        <?php
                            echo $this->Form->textarea('post_text', array(
                                'cols' => '25',
                                'rows' => '5',
                                'id' => 'post_text',
                                'class' => 'form-control mb-3',
                            ));
                        ?>
                    </div>
                    <div class="col-md-2 mt-5">
                        <?php
                            $submit = array(
                                'label' => 'Save',
                                'class' => 'form-control btn btn-primary text-uppercase'
                            );
                        ?>
                        <?php echo $this->Form->end($submit); ?>
                    </div>
                </div>
            </div>
        </main>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
    </div>
</div>


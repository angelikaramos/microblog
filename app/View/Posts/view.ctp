<!-- File: /app/View/Posts/view.ctp -->
<?php 
    App::import('Controller', 'Users');
    App::import('Controller', 'Likes');
    App::import('Controller', 'Reposts');
    App::import('Controller', 'Comments');

    // Controllers 
    $usersController = new UsersController();
    $likesController = new LikesController();
    $repostsController = new RepostsController();
    $commentsController = new CommentsController();

    // Get comments in certain post
    $comments = $commentsController->getComments($post['Post']['id']);

    // Get logged in user_id
    $user_id = AuthComponent::user('id');
?>
<!-- Navigation Bar -->
<?php echo $this->element('navbar'); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <!-- Side Navigation Bar -->
        <?php echo $this->element('side_navbar'); ?>
    </div>
    <div id="layoutSidenav_content">
        <!-- Flash message -->
        <?php echo $this->Flash->render(); ?>
        <main>
            <div class="container-fluid">
                <div class="post mt-3">
                    <!-- Add dropdown_post element if logged in user is the one who post -->
                    <?php 
                        if (AuthComponent::user('id') == $post['User']['id']) {
                            echo $this->element('dropdown_post'); 
                        }
                    ?>
                    <div class="post-user mt-2">
                        <?php echo $this->Html->image('uploads/' . $post['User']['profile_picture'], array('alt' => 'Profile Picture', 'height' => '50px')); ?>
                        <?php echo $this->Html->link($post['User']['full_name'], array('controller' => 'users',
                        'action' => 'view', $post['User']['id']), array('class' => 'text-decoration-none text-dark font-weight-bold')) ?>
                        <span><?php echo "@" . h($post['User']['username']); ?></span>
                    </div>
                    <p class="post-content custom-select-lg"><?php echo h($post['Post']['post_text']); ?></p>
                    <p class="post-date small"><?php echo date('g:i A - j M Y', strtotime($post['Post']['modified'])); ?></p>
                    <hr>
                    <div class="btn-group-sm align-center">
                        <?php echo '<div class="d-inline p-2 bg-primary" id="btnLike'.$post['Post']['id'].'">'; ?>
                            <?php $loggedInUserLike = $likesController->checkLikeOfLoggedInUser($post['Post']['id']); ?>
                            <?php $countLikes = $likesController->countLikes($post['Post']['id']); ?>
                            <?php $countLikes <= 1 ? $btn = 'Like' : $btn = 'Likes'; ?>
                            <?php if ($loggedInUserLike === 0) : ?>
                                <?php echo "<span class='text-white'>" . "(" . $countLikes . ")" . "</span> "; ?>
                                <?php 
                                    echo $this->Js->link($btn, array('controller' => 'likes', 
                                    'action' => 'add', $post['Post']['id']) , array('update' => '#btnLike'.$post['Post']['id'].'', 'class' => 'text-decoration-none text-white')); 
                                ?>
                            <?php endif; ?>    
                            <?php if ($loggedInUserLike === 1) : ?>
                                <?php  echo "<span class='text-white'>" . "(" . $countLikes = $likesController->countLikes($post['Post']['id']) . ")" . "</span> "; ?>
                                <?php 
                                    echo $this->Js->link($btn, array('controller' => 'likes', 
                                    'action' => 'delete', $post['Post']['id']), array('update' => '#btnLike'.$post['Post']['id'].'', 'class' => 'text-decoration-none text-white')); 
                                ?>
                            <?php endif; ?>  
                        </div>
                        <div class="d-inline p-2 bg-dark">
                            <?php $countComments = $commentsController->countComments($post['Post']['id']); ?>
                            <?php $countComments <= 1 ? $btnComment = 'Comment' : $btnComment = 'Comments'; ?>
                            <?php echo "<span class='text-white'>" . "(" . $countComments . ")" . " " . $btnComment . "</span> "; ?>
                        </div>
                        <div class="d-inline p-2 bg-info">
                            <?php $loggedInUserRepost = $repostsController->checkRepostOfLoggedInUser($post['Post']['id'], AuthComponent::user('id')); ?>
                            <?php $countReposts = $repostsController->countReposts($post['Post']['id']); ?>
                            <?php $countReposts <= 1 ? $btnRepost = 'Repost' : $btnRepost = 'Reposts'; ?>
                            <?php echo "<span class='text-white'>" . "(" . $countReposts . ")" . "</span> "; ?>
                            <?php if ($loggedInUserRepost === 1) : ?>
                                <?php echo $this->Form->postLink($btnRepost, array('controller' => 'reposts', 
                                'action' => 'delete',  $post['Post']['id']), array('class' => 'text-decoration-none text-white',
                                'confirm' => 'Are you sure you want to undo repost?')) ?>
                            <?php endif; ?>
                            <?php if ($loggedInUserRepost === 0) : ?>
                                <?php echo $this->Form->postLink($btnRepost, array('controller' => 'reposts', 
                                'action' => 'add',  $post['Post']['id']), array('class' => 'text-decoration-none text-white')) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <hr>
                <!-- Add Comment -->
                <div class="row">
                    <div class="col-md-10">
                    <?php
                        $url = array('controller' => 'comments', 'action' => 'add');
                        echo $this->Form->create('Comment', array(
                            'url' => $url), array(
                                'inputDefaults' => array(
                                    'label' => false,
                                )
                            )
                        );
                    ?>
                    <?php
                        echo $this->Form->hidden('post_id', array(
                            'value' => $post['Post']['id'],
                        ));
                    ?>
                    <?php
                        echo $this->Form->textarea('comment_text', array(
                            'cols' => '20',
                            'rows' => '3',
                            'id' => 'post_text',
                            'class' => 'form-control',
                            'placeholder' => 'Comment your thoughts here..',
                            'required' => false,
                        ));
                    ?>
                    </div>
                    <div class="col-md-2 mt-3">
                        <?php
                            $submit = array(
                                'label' => 'Comment',
                                'class' => 'form-control btn btn-primary text-uppercase'
                            );
                        ?>
                        <?php echo $this->Form->end($submit); ?>
                    </div>
                </div>
                <!-- Comments -->
                <div class="row mt-3">
                    <div class="col-md-12">
                        <?php foreach ($comments as $comment) : ?>
                            <div class="flex-row comment-row">
                                <?php if ($user_id == $comment['User']['id']) : ?>
                                    <div class="dropdown">
                                        <i class="fas fa-chevron-down float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                        <div class="dropdown-menu"  
                                            aria-labelledby="dropdownMenuButton">
                                            <?php echo $this->Html->link('Edit Comment', array('controller'=>'comments','action'=>'edit',  $comment['Comment']['id']), array('class' => 'dropdown-item')); ?>
                                            <?php echo $this->Form->postLink('Delete Comment', array
                                            ('controller'=>'comments','action'=>'delete',  $comment
                                            ['Comment']['id']), array('class' => 'dropdown-item', 'confirm' => 'Are you sure want to delete this comment?')); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php echo $this->Html->image('uploads/' . $comment['User']['profile_picture'], array('alt' => 'Profile Picture', 'height' => '50px')); ?>
                                <span class="font-weight-bold">
                                    <?php echo $this->Html->link($comment['User']['full_name'], array('controller' => 'users', 'action' => 'view', $comment['User']['id']), array('class' => 'text-decoration-none text-dark font-weight-bold')); ?>
                                </span>
                                <span><?php echo "@" . h($comment['User']['username']); ?></span>
                                <p class="card-text"><?php echo h($comment['Comment']['comment_text']); ?></p>
                                <p class="comment-date small"><?php echo date('g:i A - j M Y', strtotime( $comment['Comment']['modified'])); ?></p><hr>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </main>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
    </div>
</div>

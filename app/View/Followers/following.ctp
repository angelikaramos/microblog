<!-- File: /app/View/Followers/following.ctp -->
<!-- Navigation Bar -->
<?php echo $this->element('navbar'); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <!-- Side Navigation Bar -->
        <?php echo $this->element('side_navbar'); ?>
    </div>
    <div id="layoutSidenav_content">
        <!-- Flash message -->
        <?php echo $this->Flash->render(); ?>
        <main>
            <div class="container-fluid">
                <h2 class="mt-4">Following</h2>
                <?php 
                    // If no following
                    if (empty($usersfollowing)) {
                        echo "<p class='text-center'> Start following people and see their posts. </p>";
                    }
                ?>
                <?php foreach ($usersfollowing as $userfollow): ?>
                    <!-- Get user data by follower_user_id -->
                    <?php $user = $this->requestAction('users/getUserById/' . $userfollow['Follower']['user_id']); ?>
                    <!-- User data -->
                    <div class="following mt-4">
                        <?php echo $this->Html->image(
                            'uploads/' . $user['User']['profile_picture'], array(
                            'alt' => 'Profile Picture', 'height' => '50px')
                        ); ?>
                        <strong><?php echo $this->Html->link($user['User']['full_name'], array('controller' => 'users',
                        'action' => 'view', $user['User']['id']), array('class' => 'text-decoration-none text-dark')) ?></strong>
                        <span><?php echo "@" . $user['User']['username']; ?></span>
                        <!-- Following Button -->
                        <?php echo $this->Form->postLink('Following', array('controller' => 'followers', 
                        'action' => 'delete',  $userfollow['Follower']['id']), array('class' => 'btn btn-primary active float-right',
                        'confirm' => 'Are you sure you want to unfollow this person?')) ?>
                    </div><hr>
                <?php endforeach; ?>
            </div>
        </main>
        <!-- Pagination -->
        <?php echo $this->element('pagination'); ?>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
    </div>
</div>

<!-- File: /app/View/Followers/follower.ctp -->
<!-- Navigation Bar -->
<?php echo $this->element('navbar'); ?>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <!-- Side Navigation Bar -->
        <?php echo $this->element('side_navbar'); ?>
    </div>
    <div id="layoutSidenav_content">
        <!-- Flash message -->
        <?php echo $this->Flash->render(); ?>
        <main>
            <div class="container-fluid">
                <h2 class="mt-4">Followers</h2>
                <?php 
                    // If no followers
                    if (empty($userfollowers)) {
                        echo "<p class='text-center'> 
                        When people follow you. It will show here. </p>";
                    }
                ?>
                <?php foreach ($userfollowers as $userfollower): ?>
                    <!-- Get user data by follower_user_id -->
                    <?php $user = $this->requestAction('users/getUserById/' . $userfollower['Follower']['follower_user_id']); ?>
                    <div class="following mt-4">
                        <!-- User data -->
                        <?php echo $this->Html->image(
                            'uploads/' . $user['User']['profile_picture'], array(
                            'alt' => 'Profile Picture', 'height' => '50px')
                        ); ?>
                        <strong><?php echo $this->Html->link($user['User']['full_name'], array('controller' => 'users',
                        'action' => 'view', $user['User']['id']), array('class' => 'text-decoration-none text-dark')) ?></strong>
                        <span><?php echo "@" . $user['User']['username']; ?></span>
                        <!-- Check if logged in user follows the follower -->
                        <?php 
                            $userfollowing = $this->requestAction('followers/checkIfFollowing/' . $userfollower['Follower']['follower_user_id']);
                        ?>
                        <!-- If $userfollowing is existing in database -->
                        <?php if (!empty($userfollowing)) : ?>
                            <?php if ($userfollowing['Follower']['_is_deleted'] === '1') : ?>
                                <?php echo $this->Form->postLink('Follow', array(
                                    'controller' => 'followers', 'action' => 'add', 
                                    $userfollower['Follower']['follower_user_id']), 
                                    array('class' => 'btn btn-primary float-right')
                                ); ?>
                            <?php endif; ?>
                            <?php if ($userfollowing['Follower']['_is_deleted'] === '0') : ?>
                                <?php echo $this->Form->postLink('Following', array(
                                    'controller' => 'followers', 'action' => 'delete',  
                                    $userfollowing['Follower']['id']), 
                                    array('class' => 'btn btn-primary active float-right',
                                    'confirm' => 'Are you sure you want to unfollow this person?')
                                ); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <!-- If $userfollowing is not yet exiting in database -->
                        <?php if (empty($userfollowing)) : ?>
                            <?php echo $this->Form->postLink('Follow', array(
                                'controller' => 'followers', 'action' => 'add', 
                                $userfollower['Follower']['follower_user_id']), 
                                array('class' => 'btn btn-primary float-right')
                            ); ?>
                        <?php endif; ?>
                    </div><hr>
                <?php endforeach; ?>
            </div>
        </main>
        <!-- Pagination -->
        <?php echo $this->element('pagination'); ?>
        <!-- Footer -->
        <?php echo $this->element('footer'); ?>
    </div>
</div>

<div class="btn-group-sm float-right">
    <?php echo '<div class="d-inline p-2 bg-primary" id="btnLike' . $post_id . '">'; ?>
        <?php 
            // Chech if logged in user like certain post
            $loggedInUserLike = $this->requestAction('likes/checkLikeOfLoggedInUser/' . $post_id);
            // Count total number of likes
            $countLikes = $this->requestAction('likes/countLikes/' . $post_id);
        ?>
        <?php $countLikes <= 1 ? $btnLike = 'Like' : $btnLike = 'Likes'; ?>
        <?php echo "<span class='text-white'>" . "(" . $countLikes . ")" . "</span> "; ?>
        <?php if ($loggedInUserLike === 0) : ?>
            <?php 
                echo $this->Js->link($btnLike, array('controller' => 'likes', 
                'action' => 'add', $post_id) , array('async' => true, 'update' => '#btnLike' . $post_id . '', 'class' => 'text-decoration-none text-white')); 
            ?>
        <?php endif; ?>    
        <?php if ($loggedInUserLike === 1) : ?>
            <?php 
                echo $this->Js->link($btnLike, array('controller' => 'likes', 
                'action' => 'delete', $post_id), array('async' => true, 'update' => '#btnLike' . $post_id . '', 'class' => 'text-decoration-none text-white')); 
            ?>
        <?php endif; ?>
    </div>
    <div class="d-inline p-2 bg-dark">
        <?php 
            // Count total number of comments
            $countComments = $this->requestAction('comments/countComments/' . $post_id);
        ?>
        <?php $countComments <= 1 ? $btnComment = 'Comment' : $btnComment = 'Comments'; ?>
        <?php echo "<span class='text-white'>" . "(" . $countComments . ")" . "</span> "; ?>
        <?php echo $this->Form->postLink($btnComment, array('controller' => 'posts', 
        'action' => 'view',  $post_id), array('class' => 'text-decoration-none text-white')) ?>
    </div>
    <div class="d-inline p-2 bg-info">
        <?php 
            // Chech if logged in user repost certain post
            $loggedInUserRepost = $this->requestAction('reposts/checkRepostOfLoggedInUser/' . $post_id . "/" . $user_id);
            // Count total number of reposts
            $countReposts = $this->requestAction('reposts/countReposts/' . $post_id . "/" . $user_id)
        ?>
        <?php $countReposts <= 1 ? $btnRepost = 'Repost' : $btnRepost = 'Reposts'; ?>
        <?php echo "<span class='text-white'>" . "(" . $countReposts . ")" . "</span> "; ?>
        <?php if ($loggedInUserRepost === 1) : ?>
            <?php echo $this->Form->postLink($btnRepost, array('controller' => 'reposts', 
            'action' => 'delete',  $post_id), array('class' => 'text-decoration-none text-white',
            'confirm' => 'Are you sure you want to undo repost?')) ?>
        <?php endif; ?>
        <?php if ($loggedInUserRepost === 0) : ?>
            <?php echo $this->Form->postLink($btnRepost, array('controller' => 'reposts', 
            'action' => 'add',  $post_id), array('class' => 'text-decoration-none text-white')) ?>
        <?php endif; ?>
    </div>
</div>
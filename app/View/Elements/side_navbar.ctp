<nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            <a href="<?= $this->Html->url(array('controller' => 'posts', 'action' => 'index'), 
                array('escape' => false)); ?>" class="nav-link">
                <div class="sb-nav-link-icon"></div>
                <strong>HOME</strong>
            </a>
            <a href="<?= $this->Html->url(array('controller' => 'users', 'action' => 'view'), 
                array('escape' => false)); ?>" class="nav-link">
                <div class="sb-nav-link-icon"></div>
                <strong>PROFILE</strong>
            </a>
            <a href="<?= $this->Html->url(array('controller' => 'followers', 'action' => 'following'), 
                array('escape' => false)); ?>" class="nav-link">
                <div class="sb-nav-link-icon"></div>
                <strong>FOLLOWING</strong>
            </a>
            <a href="<?= $this->Html->url(array
                ('controller' => 'followers', 'action' => 'follower'), 
                array('escape' => false)); ?>" class="nav-link">
                <div class="sb-nav-link-icon"></div>
                <strong>FOLLOWERS</strong>
            </a>
            <br>
            <!-- Who to follow Section -->
            <!-- Get users not yet followed -->
            <?php $usersNotFollowed = $this->requestAction('users/getUsersNotYetFollowed'); ?>
            <div class="sb-nav">
                <strong>Who to follow:</strong><br><br>
                <?php 
                    if (empty($usersNotFollowed)) {
                        echo "<p class='text-center'> People who are not yet followed will show here. </p>";
                    }
                ?>
                <?php foreach ($usersNotFollowed as $userNotFollowed): ?>
                    <?php $baseUrl = array('controller' => 'followers', 'action' => 'add');  ?>
                    <?php echo $this->Html->link($userNotFollowed['User']['full_name'], array
                        ('controller' => 'users', 'action' => 'view', $userNotFollowed['User']['id']), array('class' => 'text-decoration-none text-dark font-weight-bold')
                    ); ?><br>
                    <span class="ml-2 small"> <?php echo "@" . $userNotFollowed['User']['username']; ?> </span>
                    <?php echo $this->Form->postLink('Follow', array('controller' => 'followers', 
                    'action' => 'add',  $userNotFollowed['User']['id']),
                     array('class' => 'btn btn-primary btn-sm float-right mr-2 mb-2')) ?><br>
                    <br>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="sb-sidenav-footer">
        <div class="small">Logged in as:</div>
        <h5><?php echo AuthComponent::user('full_name'); ?></h5>
    </div>
</nav>
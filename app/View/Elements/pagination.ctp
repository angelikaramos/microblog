<div class="col-md-12">
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <li class="page-item">
                <?php echo $this->Paginator->prev('Prev', array('class' => 'page-link'), 
                null, array('class' => 'page-link prev disabled')); ?>
            </li>
            <?php echo $this->Paginator->numbers(array(
                'separator' => '','currentTag' => 'a', 
                'class' => 'page-link'), 
                array('tag' => 'li', 'class' => 'page-item')
            ); ?>
            <li class="page-item">
                <?php echo $this->Paginator->next('Next', array('class' => 'page-link'), 
                null, array('class' => 'page-link next disabled')); ?>
            </li>
        </ul>
    </nav>                                        
</div>
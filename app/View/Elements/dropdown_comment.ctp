<!-- Dropdown Comment -->
<div class="dropdown">
    <i class="fas fa-chevron-down float-right" type="button" id="dropdownMenuButton" 
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <?php echo $this->Html->link('Edit Comment', array(
            'controller'=>'comments','action'=>'edit',  $comment['Comment']['id']
            ), array('class' => 'dropdown-item')
        ); ?>
        <?php echo $this->Form->postLink('Delete Comment', array(
            'controller'=>'comments','action'=>'delete',  $comment['Comment']['id']),
            array('class' => 'dropdown-item', 'confirm' => 'Are you sure want to delete this post?')
        ); ?>
    </div>
</div>
<!-- Dropdown Post -->
<div class="dropdown">
    <i class="fas fa-chevron-down float-right" type="button" id="dropdownMenuButton" 
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <?php echo $this->Html->link('Edit Post', array(
            'controller'=>'posts','action'=>'edit',  $post['Post']['id']),
            array('class' => 'dropdown-item')
        ); ?>
        <?php echo $this->Form->postLink('Delete Post', array(
            'controller'=>'posts','action'=>'delete',  $post['Post']['id']),
            array('class' => 'dropdown-item', 'confirm' => 'Are you sure want to delete this post?')
        ); ?>
    </div>
</div>
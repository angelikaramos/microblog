<!-- Navigation Bar -->
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-light">
    <!-- Title -->
    <h4 class="ml-4">MICROBLOG</h4>
    <!-- Side Bar Toggle Button -->
    <button class="btn btn-link btn-sm order-1 order-lg-0 bg-d text-dark" id="sidebarToggle" href="#"> 
        <i class="fas fa-bars"></i>
    </button>
    <!-- Search bar -->
    <?php
    $url = array('controller' => 'users', 'action' => 'search');
        echo $this->Form->create('User', array(
            'url' => $url, 'class' => 'd-md-inline-block form-inline mx-auto', 'autocomplete' => 'off'), array(
                'inputDefaults' => array(
                    'label' => false,
                ) 
            )
        );
    ?>
    <?php
        if (isset($keyword)) {
            $search_keyword =  $keyword;
        }
        if (empty($keyword)) {
            $search_keyword = "";
        }
        if (isset($this->request->query['keyword'])){
            $search_keyword = $this->request->query['keyword'];
        }
    ?>
    <div class="input-group">
        <?php
            echo $this->Form->input('keyword', array(
                'class' => 'form-control',
                'placeholder' => 'Search for...',
                'label' => false,
                'value' => $search_keyword,
            ));
        ?>
    
        <?php
            $submit = array(
                'label' => 'Search',
                'class' => 'btn btn-primary',
                'div' => array(
                    'class' => 'input-group-append',
                )
            );
        ?>
        <?php echo $this->Form->end($submit); ?>
    </div>
    <!-- Logout-->
    <div class="navbar-nav ml-auto ml-md-0">
        <?php
            echo $this->Html->link('LOGOUT', array(
                'controller' => 'users', 
                'action' => 'logout',
                'full_base' => true), array(
                'class' => 'text-decoration-none text-dark font-weight-bold',
                )
            );
        ?>
    </div>
</nav>
<span class="small text-primary">
    <?php 
        // Check if the certain post was reposted
        $repost = $this->requestAction('reposts/checkRepost/' . $post_id);
        // If there is a reposter
        if ($repost >= 1) {
            $getRepost = $this->requestAction('reposts/getRepost/' . $post_id);
            if ($getRepost['Repost']['reposter_user_id'] 
                !== $user_id) {
                echo "<i class='fa fa-retweet' aria-hidden='true'></i> Reposted by ";
                echo $getRepost['User']['username'];
            } elseif ($getRepost['Repost']['reposter_user_id']=== $user_id) {
                echo "<i class='fa fa-retweet' aria-hidden='true'></i> You reposted";
                // echo $getRepost['User']['username'];
            }
        }  else {
            // Else, check if the certain post was reposted by logged in user
            $repost = $this->requestAction('reposts/checkRepostOfLoggedInUser/' . $post_id);
            // If reposted by user
            if ($repost === 1) {
                // Get post_id of repost
                $getRepost = $this->requestAction('reposts/getRepostOfLoggedInUser/' . $post_id);
                // Check if the reposter_user_id is the logged in user
                $reposter = $this->requestAction('reposts/getReposterUserId/' . $getRepost['Repost']['id']);
                if ($reposter['Repost']['reposter_user_id'] === $user_id) {
                    echo " <i class='fa fa-retweet' aria-hidden='true'></i> ";
                    echo "You reposted";
                }
            }
        }
    ?>
</span>
<!-- Template for Email Verification when email was updated -->
<p>Hi <?php echo strtok($name, " ") . ","; ?></p></br>
<p>Thank you for using MICROBLOG!</p>
<p>Please click the link below to activate your account:</p>
    <?php echo Router::url(array("controller" => "users", "action" => "activate", $code),true) ?>
<p>Thanks,</p>
<p>Microblog Family</p>
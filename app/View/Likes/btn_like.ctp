<?php 
// Like Button - Check if post is noy yet liked by logged in user
$loggedInUserLike = $this->requestAction('likes/checkLikeOfLoggedInUser/' . $post_id); 
if ($loggedInUserLike === 0) {
    $countLikes = $this->requestAction('likes/countLikes/' . $post_id); 
    $countLikes <= 1 ? $btnLike = 'Like' : $btnLike = 'Likes'; 
    echo "<span class='text-white'>" . "(" . $countLikes . ")" . "</span> "; 
    
    echo $this->Js->link($btnLike, array('controller' => 'likes', 
    'action' => 'add', $post_id) , array('async' => true, 'update' => '#btnLike' . $post_id . '', 'class' => 'text-decoration-none text-white'));
}  

<?php 
// Liked Button - Check if post already liked by logged in user
$loggedInUserLike = $this->requestAction('likes/checkLikeOfLoggedInUser/' . $post_id); 
if ($loggedInUserLike === 1) {
    $countLikes = $this->requestAction('likes/countLikes/' . $post_id); 
    $countLikes <= 1 ? $btnLike = 'Like' : $btnLike = 'Likes'; 
    echo "<span class='text-white'>" . "(" . $countLikes . ")" . "</span> "; 
    
    echo $this->Js->link($btnLike, array('controller' => 'likes', 
    'action' => 'delete', $post_id), array('async' => true, 'update' => '#btnLike' . $post_id . '', 'class' => 'text-decoration-none text-white')); 
}   
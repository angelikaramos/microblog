<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Microblog');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		// CSS files
		echo $this->Html->css('bootstrap.min.css');
		echo $this->Html->css('all.min.css');
		echo $this->Html->css('styles.css');

		// JavaScript files
		echo $this->Html->script('jquery.js');
		echo $this->Html->script('navigation.js');
		// echo $this->Html->script('like.js');
		echo $this->Html->script('jquery.min.js', ['defer']);
		echo $this->Html->script('bootstrap.bundle.min.js', ['defer']);
		echo $this->Html->script('popper.min.js', ['defer']);
		echo $this->Html->script('script.js', ['defer']);

		// For AJAX requests
		echo $this->Js->writeBuffer(array('cache' => TRUE));	

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="content">
		<?php echo $this->Flash->render(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
</body>
</html>

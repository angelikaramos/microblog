$(document).ready(function () {
    $("#addLike").click(function(event) {
        // Get post_id using $(this).attr('href').split('/')[4]
        var btnLike = "#btnLike"+$(this).attr('href').split('/')[4];
        var addLikeUrl = "\/microblog\/likes\/add\/"+$(this).attr('href').split('/')[4];
        $.ajax({    
            dataType:"html", 
            success:function (data, textStatus) {
                $(btnLike).html(data);
            }, url: $(this).attr('href'),
        });
        return false;
    });
});

$(document).ready(function () {
    $("#deleteLike").click(function(event) {
        // Get post_id using $(this).attr('href').split('/')[4]
        var btnLike = "#btnLike"+$(this).attr('href').split('/')[4];
        var deleteLikeUrl = "\/microblog\/likes\/delete\/"+$(this).attr('href').split('/')[4];
        $.ajax({    
            dataType:"html", 
            success:function (data, textStatus) {
                $(btnLike).html(data);
            }, url: $(this).attr('href')
        });
        return false;
    });
});
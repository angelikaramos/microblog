<?php
App::uses('AppController', 'Controller');

class PostsController extends AppController
{
    public $helpers = array('Html', 'Form');

    public $components = array('Paginator');

    /**
     * Display posts
     * 
     * @return array
     */
    public function index() 
    {
        // Get logged in user_id
        $follower_user_id = AuthComponent::user('id');

        // Load Models
        $this->loadModel('Follower');
        $this->loadModel('Repost');
        
        // Get all the user_id of logged in user follows
        $follow = $this->Follower->find('list', array( 
            'conditions' => array(
                'AND' => array(
                'follower_user_id' => $follower_user_id, '_is_deleted' => 0)), 
            'fields' => array('user_id'),
        ));

        // Include user_id of logged in user to also view his/her reposts
        array_push($follow, $follower_user_id);

         // Check if the logged in user and he follows has reposts
        $reposts = $this->Repost->find('list', array(
            'conditions' => array(
                'AND' => array('reposter_user_id' => $follow, '_is_deleted' => 0)),
            'fields' => array('post_id'),
        ));

        // Passed the data to index page
        $this->Paginator->settings = array(
            'conditions' => array('Post._is_deleted' => 0, 
                'OR' => array('Post.user_id' => $follow, 'Post.id' => $reposts)),
            'order' => array('Post.created' => 'desc'), 'limit' => 2);

        // Similar to findAll(), but fetches paged results
        $data = $this->Paginator->paginate('Post');
        $this->set('posts', $data);
    }

    /**
     * Add Post
     * 
     * @return redirected page
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Post->create();
            
            // Get logged in user_id
            $user_id = AuthComponent::user('id');

            // Set user_id data before saving to database
            $this->Post->set('user_id', $user_id);

            $post_text = $this->request->data['Post']['post_text'];

            // Sanitize post_text input
            $post_text = strip_tags(addslashes($post_text));
            $post_text = htmlspecialchars($post_text);
            $this->Post->set('post_text', $post_text);
            
            if ($this->Post->save($this->request->data)) {
                // Flash Message
                $this->Flash->success(__('You created a post.'));
            }
            if (!$this->Post->save($this->request->data)) {
                // Flash Message
                $this->Flash->error(__('Unable to add your post.'));
            }
            return $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * View Post
     * @param $id = null
     * @return array
     */
    public function view($id = null) 
    {
        // Check if id exists
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        // Find Post by id
        $post = $this->Post->findById($id);

        // Check if post exists
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }

        // Pass post details
        $this->set('post', $post);
    }
    
    /**
     * Edit Post
     * @param $id = null
     * @return array
     */
    public function edit($id = null)
    {
        // Check if id exists
        if (!$id) {
            throw new NotFoundException(__('Invalid post'));
        }

        // Find Post by id
        $post = $this->Post->findById($id);

        // Check if post exists
        if (!$post) {
            throw new NotFoundException(__('Invalid post'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Post->id = $id;
            if ($this->Post->save($this->request->data)) {
                // Flash Message
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(array('action' => 'view', $id));
            }
            if (!$this->Post->save($this->request->data)) {
                // Flash Message
                $this->Flash->error(__('Unable to update your post'));
            }
        }
        if (!$this->request->data) {
            $this->request->data = $post;
        }
        
        // Pass Post details
        $post = $this->Post->findById($id);
        $this->set('post', $post);
    }

    /**
     * Delete Post
     * @param $id
     * @return redirected page
     */
    public function delete($id)
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        // Update deleted and _is_deleted column - *Soft Delete
        $deletePost = $this->Post->updateAll(
            array('Post.deleted' => 'NOW()', 'Post._is_deleted' => '1'), 
            array('Post.id' => $id, 'Post.user_id' => $user_id)
        );

        if ($deletePost) {
            // Flash Message
            $this->Flash->success(__('Your post has been deleted. '));

            // Get last action from url. This is when user delete a post in a user view page
            $refer_url = $this->referer('/', true);
            $parse_url_params = Router::parse($refer_url);

            if ($parse_url_params['controller'] == 'users' && $parse_url_params['action'] == 'view') {
                // Redirected to user view page
                return $this->redirect(array('controller' => 'users', 'action' => 'view'));
            }
            // Redirected to index
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }

        if (!$deletePost) {
            // Flash Message
            $this->Flash->error(__('Failed to delete post'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
    }

    /**
     * Get Post details
     * @param $id
     * @return array
     */
    public function getPost($id)
    {
        // Get Post details
        $posts = $this->Post->find('all', array(
            'conditions' => array(
                'id' => $id
            )
        ));

        // Pass Post details
        $this->set('posts', $posts);
    }

    /**
     * Count total number of posts
     * @param $user_id = null
     * @return int
     */
    public function countPosts($user_id = null)
    {
        // If $user_id is empty use logged in user id for profile.ctp
        if (empty($user_id)) {
            // Get logged in user_id
            $user_id = AuthComponent::user('id');
        }

        // Count posts of certain user
        $posts = $this->Post->find('count', array(
            'conditions' => array(
                'AND' => array('user_id' => $user_id, '_is_deleted' => 0))
        ));
        
        // Return count
        return $posts;
    }
}

<?php
App::uses('AppController', 'Controller');

class FollowersController extends AppController
{
    public $components = array('Paginator');

    /**
     * Add Follower
     * @param $user_id
     * @return redirected page
     */
    public function add($user_id)
    {
        // Get logged in user_id
        $follower_user_id = AuthComponent::user('id');

        // Check if follower data already exists
        $follower = $this->Follower->find('all', array(
            'conditions' => array(
                'AND' => array(
                    'user_id' => $user_id,
                    'follower_user_id' => $follower_user_id,
                    '_is_deleted' => array(1,0),
                ),
            ),
        ));

        // Update column deleted and _is_deleted if data already exists
        if (!empty($follower)) {
            $this->Follower->updateAll(
                ['deleted' => null, '_is_deleted' => '0', 'modified' => 'NOW()'], 
                array('user_id' =>  $user_id, 'follower_user_id' => $follower_user_id)
            );

            // Get username for flash message
            $username = $this->Follower->find('first', array(
                'conditions' => array('user_id' =>  $user_id),
            )); 

            // Flash Message
            $this->Flash->success(__('You followed @' . $username['User']['username']));

            // When followed in search page, pass the keyword back to search page after following
            if (isset($this->request->query['keyword'])) {
                // Load User Model
                $this->loadModel('User');
                $this->request->params['named']['User.keyword'] = $this->request->query['keyword']; 
                $keyword = $this->request->params['named']['User.keyword'];
                return $this->redirect(array(
                    'controller' => 'users', 'action' => 'search', 
                    '?' => array('keyword' => $keyword)
                ));
            }

            // Redirection to recent page
            if (empty($this->request->query['keyword'])) {
                return $this->redirect($this->referer());
            }
        }

        // Add data if data is not yet exists
        if (empty($follower)) {
            $this->Follower->set('user_id', $user_id);
            $this->Follower->set('follower_user_id',  $follower_user_id);
            $this->Follower->save();

            // Get username for flash message
            $username = $this->Follower->find('first', array(
                'conditions' => array('user_id' =>  $user_id),
            )); 

            // Flash Message
            $this->Flash->success(__('You followed @' . $username['User']['username']));
            
            // When followed in search page, pass the keyword back to search page after following
            if (isset($this->request->query['keyword'])) {
                $this->loadModel('User');
                $this->request->params['named']['User.keyword'] = $this->request->query['keyword']; 
                $keyword = $this->request->params['named']['User.keyword'];

                // Go back to search page
                return $this->redirect(array('controller' => 'users', 'action' => 'search', '?' => 
                array('keyword' => $keyword)));
            }
            if (empty($this->request->query['keyword'])) {
                // Go back to recent page
                return $this->redirect($this->referer());
            }
        }
        $this->Flash->error(__('Failed in following ' . $username['User']['username']));
    }

    /**
     * Get Logged in user following
     * 
     * @return array
     */
    public function following() 
    {
        // Get logged in user_id
        $follower_user_id = AuthComponent::user('id');

        // Get all logged in user's following
        $this->Paginator->settings = array(
            'conditions' => array(
                'AND' => array(
                'follower_user_id' =>  $follower_user_id, '_is_deleted !=' => '1'),
            ),
            'limit' => '4',
            'order' => array('modified' => 'desc'),
        );

        $data = $this->Paginator->paginate('Follower');
        $this->set('usersfollowing', $data);
    }

    /**
     * Get logged in user followers
     * 
     * @return array
     */
    public function follower() 
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');

        // Get all logged in user followers
        $this->Paginator->settings = array(
            'conditions' => array(
                'AND' => array('user_id' => $user_id, '_is_deleted' => 0)
            ),
            'limit' => '4',
            'order' => array('modified' => 'desc'),
        );

        $data = $this->Paginator->paginate('Follower');
        $this->set('userfollowers', $data);
    }

    /**
     * Delete Follower
     * @param $id
     * @return redirected page
     */
    public function delete($id)
    {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        
        // Get user_id 
        $user_id = $this->Follower->find('first', array(
            'conditions' => array('Follower.id' =>  $id),
        )); 

        // Load User Model
        $this->loadModel('User');

        // Get username by user_id for flash message
        $username = $this->User->find('first', array(
            'conditions' => array('id' => $user_id['Follower']['user_id']),
        ));

        // Update deleted and _is_deleted column - *Soft Delete
        $deleteFollower = $this->Follower->updateAll(
            array('Follower._is_deleted' => '1', 'Follower.deleted' => 'NOW()'), 
            array('Follower.id' => $id)
        );

        if ($deleteFollower) {
            // Flash Message
            $this->Flash->success(__('You unfollowed @' . $username['User']['username']));
        } 
        if (!$deleteFollower) {
            // Flash Message
            $this->Flash->error(__('Failed to unfollow @' . $username['User']['username']));
        }

        // When unfollowed in search page, pass the keyword back to search page after unfollowing
        if (isset($this->request->query['keyword'])) {
            $this->loadModel('User');
            $this->request->params['named']['User.keyword'] = $this->request->query['keyword']; 
            $keyword = $this->request->params['named']['User.keyword'];

            // Go back to search page
            return $this->redirect(array('controller' => 'users', 'action' => 'search', '?' => 
            array('keyword' => $keyword)));
        }

        // Get last action from url. This is when user unfollow in a page of following and there is only one user
        $refer_url = $this->referer('/', true);
        $parse_url_params = Router::parse($refer_url);

        if ($parse_url_params['action'] == 'following') {
            // Go back to first page
            return $this->redirect(array('controller' => 'followers', 'action' => 'following'));
        }
        if (empty($this->request->query['keyword'])) {
            // Go back to recent page
            return $this->redirect($this->referer());
        }
    }

    /**
     * Check if Logged in user follows certain user
     * @param $user_id
     * @return array
     */
    public function checkIfFollowing($user_id)
    {
        // Get logged in user_id
        $follower_user_id = AuthComponent::user('id');

        // Check if the the user_id followed by logged in user
        $following = $this->Follower->find('first', array(
            'conditions' => array(
                'AND' => array('Follower.user_id' => $user_id, 'Follower.follower_user_id' => $follower_user_id)
            ),
        ));

        // Return the Array
        return $following;
    }

    /**
     * Count user followers
     * @param $user_id = null
     * @return int
     */
    public function countFollowers($user_id = null)
    {
        if (empty($user_id)) {
            // Get logged in user_id
            $user_id = AuthComponent::user('id');
        }
        // Count the followers of logged in user
        $userfollowers = $this->Follower->find('count', array(
            'conditions' => array(
                'AND' => array('user_id' => $user_id, '_is_deleted' => 0)
            ),
        ));

        // Return the Count
        return $userfollowers;
    }

    /**
     * Count user following
     * @param $user_id = null
     * @return int
     */
    public function countFollowing($user_id = null)
    {
        if (empty($user_id)) {
            // Get logged in user_id
            $user_id = AuthComponent::user('id');
        }
        // Count the following of logged in user
        $userfollowers = $this->Follower->find('count', array(
            'conditions' => array(
                'AND' => array('follower_user_id' => $user_id, '_is_deleted' => 0)),
        ));

        // Return the Count
        return $userfollowers;
    }
}
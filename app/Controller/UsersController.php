<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController
{
    public $components = array('Paginator');

    /**
     * Executed before every action
     * 
     * @return redirected page
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow users to register, email activation.
        $this->Auth->allow('add', 'activate');
    }

    /**
     * Add user
     * 
     * @return redirected page
     */
    public function add() 
    {
        // Will not redirect to register page if already logged in
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        if ($this->request->is('post')) {
            $this->User->create();

            // Get data from form for email activation
            $name = $this->request->data['User']['full_name'];
            $email = $this->request->data['User']['email'];

            // Set code_of_activation value before saving to database
            $bytes = random_bytes(10);
            $code_of_activation = bin2hex($bytes);
            $this->User->set('code_of_activation', $code_of_activation);
            $this->User->set('profile_picture', 'profile_picture.png');

            // Send link for email verification
            $Email = new CakeEmail('gmail');

            if ($this->User->save($this->request->data)) {
                // Get template from View/Emails/email_verification.ctp
                $Email->template('email_verification');
                $Email->emailFormat('html');
                $Email->from(array('microblogfamily@gmail.com' => 'MICROBLOG'));
                $Email->to($email);
                $Email->subject('Email Verification');
                $Email->viewVars(array('name' => $name, 'code' => $code_of_activation));
                
                if ($Email->send()) {
                     // Flash message
                    $this->Flash->success(__('Registration success.
                    A link has been sent to your email to activate your account.'));
                } else {
                    // Flash message
                   $this->Flash->error(__('Failed in sending link to your email. Please try again.'));
               }
            }
            if (!$this->User->save($this->request->data)) {
                $this->Flash->error(__('Registration failed, Please try again.'));
            }
        }
    }

    /**
     * Edit user
     * @param id = null
     * @return redirected page
     */
    public function edit($id = null) 
    {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }

        // Get user details
        $user = $this->User->find('first', array(
            'conditions' => array('id' => AuthComponent::user('id'))
        ));

        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data['User']['profile_picture']['name'])) {
                $frmData = $this->request->data;  
                $tmp = $frmData['User']['profile_picture']['tmp_name']; 
                $image = $frmData['User']['profile_picture']['name'];
                $target = WWW_ROOT.'img'.DS.'uploads'.DS; 
                $target = $target.basename($image); 
                move_uploaded_file($tmp, $target);

                // Set profile_picture
                $this->request->data['User']['profile_picture'] 
                = $this->request->data['User']['profile_picture']['name'];
                $this->User->set('profile_picture',  $this->request->data['User']['profile_picture']); 
            } 

            // Save password only without changing profile_picture
            if (empty($this->request->data['User']['profile_picture']['name'])) {
                unset($this->request->data['User']['profile_picture']);
            }

            // Save password
            if (!empty($this->request->data['User']['change_password'])) {
                $this->User->set('password', ($this->request->data['User']['change_password'])); 
            }

            // Save email
            if ($user['User']['email'] !== $this->request->data['User']['email']) {
                // Set code_of_activation value before saving to database
                $bytes = random_bytes(10);
                $code_of_activation = bin2hex($bytes);
                $this->User->set('code_of_activation', $code_of_activation);
                $this->User->set('_is_email_activated', 0);
                $this->User->set('email', ($this->request->data['User']['email']));
            }

            // Sanitize username input
            $username = $this->request->data['User']['username'];
            $username = strip_tags(addslashes($username));
            $username = htmlspecialchars($username);
            $this->User->set('username', $username);

            if ($this->User->save($this->request->data)) {
                if ($user['User']['email'] !== $this->request->data['User']['email']) {
                    // Get data from form for email activation
                    $name = $user['User']['full_name'];
                    $email = $this->request->data['User']['email'];
                    
                    // Send link for email verification
                    $Email = new CakeEmail('gmail');

                    // Get template from View/Emails/email_updated.ctp
                    $Email->template('email_updated');
                    $Email->emailFormat('html');
                    $Email->from(array('microblogfamily@gmail.com' => 'MICROBLOG'));
                    $Email->to($email);
                    $Email->subject('Email Verification');
                    $Email->viewVars(array('name' => $name, 'code' => $code_of_activation));
                    
                    if ($Email->send()) {
                        // Flash message
                        $this->Flash->success(__('Your profile has been updated.
                        A link has been sent to your email to activate your account.'));
                        return $this->redirect(array('action' => 'logout'));
                    }
                }
                if (!empty($this->request->data['User']['change_password'])) {
                    // Flash message
                    $this->Flash->success(__('Your profile has been updated.
                    Please login and use your new password.'));
                    return $this->redirect(array('action' => 'logout'));
                }
                // Flash message
                $this->Flash->success(__('Your profile has been updated'));
                return $this->redirect(array('action' => 'view'));
            }
            else {
                // Flash message
                $this->Flash->error( __('The user could not be saved. Please try again'));
            }
        } 
        else {
            $this->request->data = $this->User->findById($id);
            unset($this->request->data['User']['password']);
        }
    }

    /**
     * Activate account of user
     * @param $code_of_activation
     * @return redirected page
     */
    public function activate($code_of_activation)
    {
        if (!empty($code_of_activation)) {
            
            // Get user data where code_of_activation is equal to param and _is_email_activated = 0
            $getUser = $this->User->find('first', array(
                'conditions' => array(
                    'code_of_activation' => $code_of_activation,         
                    '_is_email_activated' => 0,
                ),
            ));

            if (!empty($getUser)) {
                // Get user id from $getUser array
                $getUserId = $getUser['User']['id'];

                // Update _is_email_activated and code_of_activation
                $data = array(
                    'id' => $getUserId, 
                    'code_of_activation' => null,  
                    '_is_email_activated' => 1,
                );
                $updateIsEmailActivated = $this->User->save($data);
                
                if ($updateIsEmailActivated) {
                    // Flash message
                    $this->Flash->success(__(
                        'Your account has been Activated successfully. Please login.')
                    );
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
                if (!$updateIsEmailActivated) {
                    // Flash message
                    $this->Flash->error(__('Failed in activating your account. Please, try again'));
                    return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
            }
        }
    }

    /**
     * Login user account
     * 
     * @return redirected page
     */
    public function login()
    {   
        // Will not redirect to login page if already logged in
        if($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }
        if ($this->request->is('post')) {
            // Get username data from form
            $username = $this->request->data['User']['username'];

            // Check if username is existing using count
            $count = $this->User->find('count', array(
                'conditions' => array('username' => $username)
            ));

            // If username doesnt exists
            if ($count == 0) {
                // Flash Message
                return $this->Flash->error(__('Username doesn\'t exist.'));
            }
            
            // Get other data to database and check if email is activated or not
            $getUser = $this->User->find('first', array(
                'conditions' => array('username' => $username)
            ));
            $getUserIsEmailActivated = $getUser['User']['_is_email_activated'];
            $getUserEmail = $getUser['User']['email'];

            // If email is not activated
            if ($getUserIsEmailActivated == 0) {
                // Flash Message
                $this->Flash->error(__('Please activate your account. 
                A link has been sent to: ' .$getUserEmail. ''));
            }

            // If email is activated
            if ($getUserIsEmailActivated == 1) {
                if ($this->Auth->login()) {
                    return $this->redirect($this->Auth->redirectUrl());
                }
                if (!$this->Auth->login()) {
                    // Flash Message
                    return $this->Flash->error(__(
                        'Invalid username or password. Please try again.')
                    );
                }
            }
        }
    }

    /**
     * View User 
     * @param $user_id = null
     * @return array
     */
    public function view($user_id = null) 
    {
        if (empty($user_id)) {
            // Get logged in user_id
            $user_id = AuthComponent::user('id');
        }

        $this->User->id = $user_id;
        // Check if user_id exists
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        
        // Load Models
        $this->loadModel('Repost');
        $this->loadModel('Post');

        // Get all the repost_id's of logged in user reposts
        $getLoggedInUserRepostIds = $this->Repost->find('list', array(
            'conditions' => array (
                'AND' => array('_is_deleted' => '0', 'reposter_user_id' => $user_id)),
            'fields' => array('post_id'),
        ));

        $this->Paginator->settings = array(
            'conditions' => array(
                'AND' => array('Post._is_deleted' => 0),
                'OR' => array('Post.user_id' => $user_id, 'Post.id' => $getLoggedInUserRepostIds)),
            'order' => array('Post.created' => 'desc'),
            'limit' => 2,
        );

        $data = $this->Paginator->paginate('Post');
        $this->set('user_id', $user_id);
        $this->set('posts', $data);
    }

    /**
     * Logout user account
     * 
     * @return redirected page
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Get user details by id
     * @param $id
     * @return array
     */
    public function getUserById($id) 
    {
        // Get user details by id
        $data = $this->User->findById($id);

        return $data; 
    }

    /**
     * Get logged in user not yet followed
     * @param $id
     * @return array
     */
    public function getUsersNotYetFollowed()
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');

        // Load Follower Model
        $this->loadModel('Follower');
        
        // Get the list of users that logged in user follows
        $conditions = array('AND' => array('follower_user_id' => $user_id),'_is_deleted !=' => '1');
        $usersFollowed = $this->Follower->find('list', array(
            'conditions' => $conditions,
            'fields' => array('user_id'),
        ));

        // Add the user_id of the user to be except in selecting to usersNotYetFollowed
        array_push($usersFollowed, $user_id);
        
        // Get all the users that are not followed by logged in user and account is activated
        $usersNotFollowed = $this->User->find('all',  array(
            'conditions' =>array('AND' => 
            array('NOT' => array('id' => $usersFollowed)),
            array('_is_email_activated !=' => '0')),
            'limit' => '3', 
            'order' => 'rand()')
        );

        return $usersNotFollowed;
    }

    /**
     * Search user
     * 
     * @return array
     */
    public function search()
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');

        if (!empty($this->request->data['User']['keyword'])) {
            $keyword = $this->request->data['User']['keyword'];

            // Sanitize keyword input
            $keyword = strip_tags(addslashes($keyword));
            $keyword = htmlspecialchars($keyword);

            $condition = array();
            $condition['User.full_name LIKE'] = "%" . trim($keyword) . "%";
            $condition['User.username LIKE'] = "%" . trim($keyword) . "%";
            $conditionsLogic['OR'] = $condition;

            $this->request->params['named']['User.keyword'] = $keyword;  
        }

        // Next page
        if (isset($this->request->params['named']['User.keyword'])) {
            $keyword = $this->request->params['named']['User.keyword'];
            
            $condition = array();
            $condition['User.full_name LIKE'] = "%" . trim($keyword) . "%";
            $condition['User.username LIKE'] = "%" . trim($keyword) . "%";
            $conditionsLogic['OR'] = $condition;
        }
        
        // Following result user
        if (isset($this->request->query['keyword'])) {
            $condition = array();
            $condition['User.full_name LIKE'] = "%" . trim($this->request->query['keyword']) . "%";
            $condition['User.username LIKE'] = "%" . trim($this->request->query['keyword']) . "%";
            $conditionsLogic['OR'] = $condition;

            $this->request->params['named']['User.keyword'] = $this->request->query['keyword']; 
        }

        $this->Paginator->settings = array('conditions' => array(
            'OR' => array('id !=' => $user_id), $conditionsLogic), 'limit' => '5');

        $result = $this->Paginator->paginate('User');
        $this->set('keyword', $this->request->params['named']['User.keyword']);
        // Pass data
        $this->set('data', $result);
    }
}

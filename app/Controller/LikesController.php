<?php
App::uses('AppController', 'Controller');

class LikesController extends AppController
{
    public $helpers = array('Js' => array('Jquery'));

    public $components = array('RequestHandler');

    /**
     * Add Like
     * @param $post_id
     * @return ajax view or redirected page
     */
    public function add($post_id)
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');

        // Check if like data already exists
        $liked = $this->Like->find('all', array(
            'conditions' => array(
                'AND' => array('liker_user_id' => $user_id), 'post_id' => $post_id),
            )
        );

        // Update column deleted and _is_deleted when data already exists
        if (!empty($liked)) {
            $this->Like->updateAll(array('deleted' => null, '_is_deleted' => 0), 
            array('AND' => array('post_id' => $post_id, 'liker_user_id' => $user_id)));
            
            if ($this->request->is(array('ajax'))) {
                // Handle ajax
                $this->set('post_id', $post_id);
                $this->render('btn_liked', 'ajax');
            } else {
                return $this->redirect($this->referer());
            }
        }

        // Add data if data is not yet exists
        if (empty($liked)) {
            $this->Like->set('liker_user_id', $user_id);
            $this->Like->set('post_id',  $post_id);
            $this->Like->save();

            if ($this->request->is(array('ajax'))) {
                // Handle ajax
                $this->set('post_id', $post_id);
                $this->render('btn_liked', 'ajax');
            } else {
                return $this->redirect($this->referer());
            }
        }
    }

    /**
     * Delete Like
     * @param $post_id
     * @return ajax view or redirected page
     */
    public function delete($post_id)
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');
        
        // Update deleted and _is_deleted column - *Soft Delete
        $deleteLike = $this->Like->updateAll(
            array('deleted' => 'NOW()', '_is_deleted' => '1'),
            array( 'post_id' => $post_id, 'liker_user_id' => $user_id)
        );

        if ($deleteLike) {
            if ($this->request->is(array('ajax'))) {
                // Handle ajax
                $this->set('post_id', $post_id);
                $this->render('btn_like', 'ajax');
            } else {
                return $this->redirect($this->referer());
            }
        }

        if (!$deleteLike) {
            // Flash message
            $this->Flash->error(__('Failed to unlike'));
            return $this->redirect($this->referer());
        }
    }

    /**
     * Check if liked by Logged in user
     * @param $post_id
     * @return int
     */
    public function checkLikeOfLoggedInUser($post_id)
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');

        // Check if logged in user like a certain post
        $liked = $this->Like->find('count', array(
            'conditions' => array(
                'AND' => array(
                    'liker_user_id' => $user_id, 
                    'post_id' => $post_id, 
                    '_is_deleted' => 0),
                ),
            )
        );

        // Return count
        return $liked;
    }

    /**
     * Count total number of likes
     * @param $post_id
     * @return int
     */
    public function countLikes($post_id)
    {
        // Count likes in certain post
        $likes = $this->Like->find('count', array(
            'conditions' => array(
                'AND' => array('post_id' => $post_id, '_is_deleted' => 0),
            ),
        ));

        // Return count
        return $likes;
    }
}
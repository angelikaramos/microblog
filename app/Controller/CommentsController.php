<?php
App::uses('AppController', 'Controller');

class CommentsController extends AppController
{
    /**
     * Add comment
     * 
     * @return redirected page
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $this->Comment->create();
            
            // Get logged in user_id
            $user_id = AuthComponent::user('id');

            // Set commenter_user_id before saving to database
            $this->Comment->set('commenter_user_id', $user_id);
            $comment_text = $this->request->data['Comment']['comment_text'];

            // Sanitize comment_text input before saving to database
            $comment_text = strip_tags(addslashes($comment_text));
            $comment_text = htmlspecialchars($comment_text);
            $this->Comment->set('comment_text', $comment_text);

            if ($this->Comment->save($this->request->data)) {
                // Flash message
                $this->Flash->success(__('You added a comment.'));
                return $this->redirect($this->referer());
            }
            if (!$this->Comment->save($this->request->data)) {
                // Flash message
                $this->Flash->error(__('Unable to add your comment.'));
                return $this->redirect($this->referer());
            }
        }
    }

    /**
     * Get Comments
     * @param $post_id
     * @return array
     */
    public function getComments($post_id)
    {
        // Get comments in certain post
        $comments = $this->Comment->find('all', array(
            'conditions' => array(
                'AND' => array('Comment.post_id' => $post_id, 'Comment._is_deleted' => 0),
            ),
        ));

        return $comments;
    }

    /**
     * Edit Comment
     * @param $id
     * @return array
     */
    public function edit($id)
    {
        // Check if id exists
        if (!$id) {
            throw new NotFoundException(__('Invalid comment'));
        }

        // Find Comment by id
        $comment = $this->Comment->findById($id);

        // Check if comment exists
        if (!$comment) {
            throw new NotFoundException(__('Invalid comment'));
        }

        // Update comment data
        if ($this->request->is(array('post', 'put'))) {
            $this->Comment->id = $id;
            if ($this->Comment->save($this->request->data)) {
                // Flash Message
                $this->Flash->success(__('Your comment has been updated.'));

                // Get post_id to passed in redirection to view
                $getPostId = $this->Comment->find('first', array(
                    'conditions' => array('Comment.id' => $id)
                ));

                return $this->redirect(array(
                    'controller' => 'posts', 'action' => 'view', $getPostId['Comment']['post_id'],
                ));
            }
            if (!$this->Comment->save($this->request->data)) {
                $this->Flash->error(__('Unable to update your post'));
            }
        }
        if (!$this->request->data) {
            $this->request->data = $comment;
        }
        // Pass comment details
        $comment = $this->Comment->findById($id);
        $this->set('comment', $comment);
    }

    /**
     * Delete Comment
     * @param $id
     * @return redirected page
     */
    public function delete($id)
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');
        
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }

        // Update deleted and _is_deleted column - *Soft Delete
        $deleteComment = $this->Comment->updateAll(
            array('deleted' => 'NOW()', '_is_deleted' => '1'), 
            array('Comment.id' => $id, 'Comment.commenter_user_id' => $user_id)
        );

        // Get post_id to passed in redirection to view
        $getPostId = $this->Comment->find('first', array(
            'conditions' => array('Comment.id' => $id),
        ));

        // If soft deletion succeed
        if ($deleteComment) {
            // Flash Message
            $this->Flash->success(__('Your comment has been deleted. '));
        }

        // If soft deletion fails
        if (!$deleteComment) {
            // Flash Message
            $this->Flash->error(__('Failed to delete comment'));
        }

        return $this->redirect(array(
            'controller' => 'posts', 'action' => 'view', $getPostId['Comment']['post_id'],
        ));
    }

    /**
     * Count Comments
     * @param $post_id
     * @return int
     */
    public function countComments($post_id)
    {
        // Count comments in certain post
        $comments = $this->Comment->find('count', array(
            'conditions' => array(
                'AND' => array('Comment.post_id' => $post_id, 'Comment._is_deleted' => 0)),
        ));

        return $comments;
    }
}
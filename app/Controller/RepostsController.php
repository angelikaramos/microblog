<?php
App::uses('AppController', 'Controller');

class RepostsController extends AppController 
{
    /**
     * Add Repost
     * @param $post_id
     * @return redirected page
     */
    public function add($post_id)
    {   
        // Get logged in user_id
        $user_id = AuthComponent::user('id');

        // Check if repost data already exists in database
        $repost = $this->Repost->find('all', array(
            'conditions' => array(
                'AND' => array('Repost.post_id' => $post_id, 'Repost.reposter_user_id' => $user_id)),
        ));
        
        // If data does exist, Update repost data 
        if (!empty($repost)) {
            $this->Repost->updateAll(
                ['Repost.deleted' => null, 'Repost._is_deleted' => 0], 
                array('Repost.post_id' => $post_id, 'Repost.reposter_user_id' => $user_id)
            );
        }
        // If data does not exist, Add repost data
        if (empty($repost)) {
            $this->Repost->set('post_id', $post_id);
            $this->Repost->set('reposter_user_id',  AuthComponent::user('id'));
            $this->Repost->save();
        }   

        return $this->redirect($this->referer());
    }

    /**
     * Check if Post is reposted by all the user that logged in user follows
     * @param $post_id, $id = null
     * @return int
     */
    public function checkRepost($post_id, $user_id = null)
    {
        // No need to get user follows when viewing the other users profile, for:view.ctp
        if (!empty($user_id)) {
            $reposter = $user_id;

            // Check if a certain post is reposted by all the user that logged in user follows
            $reposts = $this->Repost->find('count', array(
                'conditions' => array(
                    'AND' => array('Repost.post_id' => $post_id, 'Repost.reposter_user_id' => $reposter, 'Repost._is_deleted' => '0')),
            ));
        }

        // Get user follows before checking if a certain post is reposted, for:index.ctp
        if (empty($user_id)) {
            // Get logged in user_id
            $user_id = AuthComponent::user('id');

            // Load Follower Model
            $this->loadModel('Follower');

            // Get all the user_id of logged in user follows
            $reposter = $this->Follower->find('list', array( 
                'conditions' => array(
                    'AND' => array('follower_user_id' => $user_id),
                    array('_is_deleted' => 0),
                ), 
                'fields' => array('user_id'),
            ));

            // Check if a certain post is reposted by all the user that logged in user follows
            $reposts = $this->Repost->find('count', array(
                'conditions' => array(
                    'AND' => array('Repost.post_id' => $post_id, 'Repost.reposter_user_id' => $reposter, 'Repost._is_deleted' => '0')),
            ));
        }
        
        // Return count
        return $reposts;
    }

    /**
     * Check if Post is reposted by logged in user
     * @param $post_id
     * @return int
     */
    public function checkRepostOfLoggedInUser($post_id)
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');
        
        // Check if a certain post is reposted by logged in user
        $reposts = $this->Repost->find('count', array(
            'conditions' => array(
                'AND' => array('Repost.post_id' => $post_id, 'Repost.reposter_user_id' => $user_id, 'Repost._is_deleted' => '0')),
        ));

        // Return count
        return $reposts;
    }

    /**
     * Get Repost details
     * @param $post_id, $user_id = null
     * @return array
     */
    public function getRepost($post_id, $user_id = null) 
    {
        // No need to get user follows when viewing the other users profile, for:view.ctp
        if (!empty($user_id)) {
            $reposter = $user_id;

            // Get repost details
            $repost = $this->Repost->find('first', array(
                'conditions' => array(
                    'AND' => array('Repost.post_id' => $post_id, 
                        'Repost.reposter_user_id' => $reposter, 
                        'Repost._is_deleted' => 0,
                    ),
                ),
                'order' => array('Repost.modified' => 'desc'),
            ));
        }

        // Get user follows before checking if a certain post is reposted, for:index.ctp
        if (empty($user_id)) {
            // Get logged in user_id
            $user_id = AuthComponent::user('id');
            
            // Load Follower Model
            $this->loadModel('Follower');

            // Get all the user_id of logged in user follows
            $reposter = $this->Follower->find('list', array( 
                'conditions' => array(
                    'AND' => array('follower_user_id' => $user_id),
                    array('_is_deleted' => 0),
                ), 
                'fields' => array('user_id'),
            ));

            // Get repost details
            $repost = $this->Repost->find('first', array(
                'conditions' => array(
                    'AND' => array('Repost.post_id' => $post_id, 
                        'Repost.reposter_user_id' => $reposter, 
                        'Repost._is_deleted' => 0,
                    ),
                ),
                'order' => array('Repost.modified' => 'desc'),
            ));
        }

        return $repost;
    }

    /**
     * Get Repost details, reposted by logged in user
     * @param $post_id
     * @return array
     */
    public function getRepostOfLoggedInUser($post_id)
    {
        // Get logged in user_id
        $user_id = AuthComponent::user('id');

        // Get repost datails
        $repost = $this->Repost->find('first', array(
            'conditions' => array(
                'AND' => array('Repost.post_id' => $post_id, 'Repost.reposter_user_id' => $user_id)
            ),
        ));
    
        return $repost;
    }

    /**
     * Delete Repost
     * @param $id
     * @return redirected page
     */
    public function delete($id)
    {   
        // Get logged in user_id
        $user_id = AuthComponent::user('id');

        $deleteRepost = $this->Repost->updateAll(
            array('Repost.deleted' => 'NOW()', 'Repost._is_deleted' => '1'), 
            array('Repost.post_id' => $id, 'Repost.reposter_user_id' => $user_id)
        );

        if ($deleteRepost) {
            // Get last action from url. This is when user undo repost in a page and there is only one post
            $refer_url = $this->referer('/', true);
            $parse_url_params = Router::parse($refer_url);

            if ($parse_url_params['action'] == 'view') {
                // Go back to first page
                return $this->redirect(array('controller' => 'users', 'action' => 'view'));
            }
            return $this->redirect($this->referer());
        }
        if (!$deleteRepost) {
            // Flash Message
            $this->Flash->error(__('Failed to undo repost'));
            return $this->redirect(array('controller' => 'posts', 'action' => 'index'));
        }
    }

    /**
     * Get reposter_user_id of certain repost
     * @param $id
     * @return array
     */
    public function getReposterUserId($id)
    {
        // Get reposter_user_id of certain repost
        $reposter_user_id = $this->Repost->find('first', array(
            'conditions' => array(
            'AND' => array('Repost.id' => $id,  'Repost._is_deleted' => 0)),
            'fields' => array('Repost.reposter_user_id'),
        ));

        return $reposter_user_id;
    }

    /**
     * Count total number of reposts
     * @param $post_id
     * @return int
     */
    public function countReposts($post_id)
    {
        // Count reposts of certain post
        $reposts = $this->Repost->find('count', array(
            'conditions' => array(
                'AND' => array('Repost.post_id' => $post_id, 'Repost._is_deleted' => 0)
            )
        ));

        // Return count
        return $reposts;
    }
}